# taric/skugrid
A Magento module for repricing

-----------------------------------------------

Installation
============

The only supported way of installing an extension, is using it as a composer dependency.

1. Add extension and firegento repositories in composer

         "repositories": [
            {
              "type": "composer",
              "url": "http://packages.firegento.com"
            },
            {
              "type": "vcs",
              "url": "git@bitbucket.org:taric/skugrid.git"
            }
        ]

2. Add extension as a requirement

        "require": {
            "taric/skugrid": "*"
        }

3. Run `composer install`

4. Clean Magento Cache


## Requirements
* Magento 1.x (1.7.0.0 or later)
* PHP 5.4 or later
* (Optional) PHP OpCache (for better performance)

### Licence

[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

## NOTES


### Copyright

(c) 2016 SkuIO (http://skugrid.com/)