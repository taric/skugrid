<?php
/**
 * SkuGrid
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO. (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SkuIO_SkuGrid_Model_Adminhtml_System_Config_Source_Frequency
{
    /**
     * Retrieve option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('skuio_skugrid');
        $frequencyArray = array(
            array('value' => 0, 'label' => $helper->__('every 30 minutes')),
            array('value' => 1, 'label' => $helper->__('every hour')),
            array('value' => 2, 'label' => $helper->__('every 2 hours')),
            array('value' => 3, 'label' => $helper->__('every 3 hours')),
            array('value' => 4, 'label' => $helper->__('every 4 hours')),
            array('value' => 5, 'label' => $helper->__('every 5 hours')),
            array('value' => 6, 'label' => $helper->__('every 6 hours')),
            array('value' => 12, 'label' => $helper->__('every 12 hours')),
        );

        return $frequencyArray;
    }
}
