<?php
/**
 * SkuGrid
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO. (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SkuIO_SkuGrid_Model_Adminhtml_System_Config_Backend_Cron extends Mage_Core_Model_Config_Data
{
    const CRON_STRING_PATH = 'crontab/jobs/skuio_skugrid/schedule/cron_expr';

    protected function _afterSave()
    {
        $frequency = Mage::getStoreConfig('skugrid/general/frequency');

        $cronExprArray = array(
            ($frequency == 0) ? '*/30' : '0',       # Minute
            ($frequency > 0) ? "*/$frequency" : '*',# Hour
            '*',                                    # Day of the Month
            '*',                                    # Month of the Year
            '*',                                    # Day of the Week
        );
        $cronExprString = join(' ', $cronExprArray);

        try {
            Mage::getModel('core/config_data')
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
        }
        catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save the cron expression.'));

        }
    }
}
