<?php
/**
 * SkuGrid
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SkuIO_SkuGrid_Model_Api extends Mage_Core_Model_Abstract
{
    private $_url;
    private $_apiKey;
    private $_client;
    private $_allowedAttributes;

    /**
     * Initialize mode
     *
     * @return void
     */
    protected function _construct()
    {
        $helper         = Mage::helper('skuio_skugrid');
        $this->_url     = 'http://skugrid.com/remoteApi/index.php';
        $this->_apiKey  = $helper->getApiKey();
        $this->_client  = new Varien_Http_Client($this->_url);
        $this->_client->setMethod(Varien_Http_Client::POST);
        $this->_allowedAttributes = array(
            'local_id',
            'vendor_url' ,
            'vendor_variant' ,
            'margin_percent',
            'margin_flat',
            'include_sh'
        );

        $this->_client->setConfig(array(
            'maxredirects'  => 5,
            'timeout'       => 30,
        ));
        $this->_client->setHeaders('Content-Type', 'text/html');
        $this->_client->setHeaders('Connection', 'Keep-Alive');
    }

    /**
     * Update all products from CSV
     *
     */
    public function updateAll()
    {
        $postData = array(
            'remoteKey' => $this->_apiKey,
            'exportCsv' => 1
        );

        $this->_client->setHeaders('Content-Length', strlen(json_encode($postData)));
        $this->_client->setParameterPost($postData);
        $response = $this->_client->request();
        $responseStatus = $response->getStatus();
        $responseBody = json_decode($response->getBody());
        if ($responseStatus == 200 && isset($responseBody->csv)) {
            $csvData = $this->_getCsvData($responseBody->csv);
            $productCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => array_keys($csvData)));
            foreach ($productCollection as $product) {
                $productId = $product->getId();
                $this->_updateProductAttributes($productId, $csvData[$productId]);
                $newPrice = Mage::helper('skuio_skugrid')->calculatePrice($product, $csvData[$productId]);
                Mage::getResourceSingleton('catalog/product_action')->updateAttributes(array($productId), array('price' => $newPrice), Mage::app()->getStore()->getStoreId());
            }

            $indexes = array('catalog_product_attribute','cataloginventory_stock');
            foreach ($indexes as $indexCode) {
                $indexer = Mage::getModel('index/indexer')->getProcessByCode($indexCode);
                $indexer->reindexEverything();

            }
        }
    }

    /**
     *  getItem API call
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function updateProduct(Mage_Catalog_Model_Product $product)
    {
        $postData = array(
            'remoteKey' => $this->_apiKey,
            'getItem'   => $product->getLocalId(),
        );
        $this->_client->setHeaders('Content-Length', strlen(json_encode($postData)));
        $this->_client->setParameterPost($postData);

        $response = $this->_client->setUrlEncodeBody(false)->request();
        $responseStatus = $response->getStatus();
        $responseBody = json_decode($response->getBody());
        if ($responseStatus == 200) {
            if (property_exists($responseBody, 'Item')) {
                $productData = array_pop(json_decode(json_encode($responseBody->Item), true));
                $this->_updateProductAttributes($product, $productData);

                return $productData;
            }
        }

        return false;
    }

    /**
     * updateItem API call
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function saveProduct(Mage_Catalog_Model_Product $product)
    {
        $productData = array(
            'reference'     => 'Magento ' . $product->getId(),
            'vendor_url'    => $product->getVendorUrl(),
            'vendor_variant'=> $product->getVendorVariant(),
            'reprice_store' => 'MAGENTO',
            'reprice_sku'   => $product->getId()
        );
        if ($product->getLocalId()) {
            $productData['local_id'] = $product->getLocalId();
        }

        $postData = array(
            'remoteKey' => $this->_apiKey,
            'updateItem' => json_encode($productData),
        );

        $this->_client->setHeaders('Content-Length', strlen(json_encode($productData)));
        $this->_client->setParameterPost($postData);
        $response = $this->_client->request();
        $responseStatus = $response->getStatus();
        $responseBody = json_decode($response->getBody());

        if ($responseStatus == 200) {
            if ($responseBody) {
                return $responseBody;
            }
        }

        return false;
    }

    /**
     * Get CSV data from URL
     *
     * @param string $csvUrl
     * @return array
     */
    private function _getCsvData($csvUrl)
    {
        $csvData = file_get_contents($csvUrl);
        $rows = explode("\n",$csvData);
        $data = array();
        $header = NULL;
        foreach($rows as $row) {
            if(!$header) {
                $header = str_getcsv($row);
            } elseif ($row != '') {
                $arrayItem = array_combine($header, str_getcsv($row));
                $data[$arrayItem['reprice_sku']] = $arrayItem;
            }
        }

        return $data;
    }


    /**
     * Save product attributes
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $attributes
     * @return bool
     */
    private function _updateProductAttributes(Mage_Catalog_Model_Product $product, $attributes)
    {
        $stockValue = null;
        $productId = $product->getId();
        foreach ($attributes as $key => $value) {
            if (in_array($key, $this->_allowedAttributes )) {
                Mage::getResourceSingleton('catalog/product_action')->updateAttributes(array($productId), array($key => $value), Mage::app()->getStore()->getStoreId());
            } elseif (in_array($key, array('vendor_stock', 'vendor_price'))) {
                if ($value == 0) {
                    $stockValue = 0;
                } else {
                    $stockValue = 1;
                }
            }
        }

        if ($stockValue !== null) {
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            $stockItem->setData('is_in_stock', $stockValue);
            $stockItem->save();
            $product->setData('stock_item',$stockItem);
            $product->setData('is_in_stock',$stockValue);
            $product->setData('is_salable',$stockValue);
        }

        return true;
    }
}
