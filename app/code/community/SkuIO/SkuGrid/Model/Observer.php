<?php
/**
 * SkuGrid Observer
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SkuIO_SkuGrid_Model_Observer
{
    /**
     *  Lock "local_id" attribute
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function catalogProductEditAction(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $product->lockAttribute('local_id');
        if (!$product->getGlobalOverride()) {
            $product->lockAttribute('margin_percent');
            $product->lockAttribute('margin_flat');
            $product->lockAttribute('include_sh');
        }

        return $this;
    }

    /**
     * Set SkuGrid price attribute
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function catalogProductSaveBefore(Varien_Event_Observer $observer)
    {
        if (Mage::helper('skuio_skugrid')->isEnabled()) {
            $product = $observer->getEvent()->getProduct();
            if ($product->isConfigurable()) {
                $children = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                foreach ($children as $child) {
                    $response = Mage::getModel('skuio_skugrid/api')->saveProduct($child);
                    if ($response->Ack == 'Success') {
                        Mage::getResourceSingleton('catalog/product_action')
                            ->updateAttributes(
                                array($child->getId()),
                                array('local_id' => $response->local_id),
                                Mage::app()->getStore()->getStoreId()
                            );
                    }
                }
            }
            $response = Mage::getModel('skuio_skugrid/api')->saveProduct($product);
            if ($response->Ack == 'Success') {
                Mage::getResourceSingleton('catalog/product_action')
                    ->updateAttributes(
                        array($product->getId()),
                        array('local_id' => $response->local_id),
                        Mage::app()->getStore()->getStoreId()
                    );
                Mage::getSingleton('adminhtml/session')->addSuccess('Product was exported to Sku Grid');
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Product was not exported to Sku Grid');
            }
        }

        return $this;
    }

    /**
     *  Update product price real time
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function catalogControllerProductView(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('skuio_skugrid');
        if ($helper->isEnabled() && $helper->isRealTime()) {
            $product = $observer->getEvent()->getProduct();
            $apiModel = Mage::getModel('skuio_skugrid/api');
            if ($product->isConfigurable()) {
                $children = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);
                foreach ($children as $child) {
                    $productData = $apiModel->updateProduct($child);
                    $this->_updatePrice($productData, $child);
                }
            }

            $productData = $apiModel->updateProduct($product);
            $this->_updatePrice($productData, $product);
        }

        return $this;
    }


    /**
     * Update price dynamically
     *
     * @param array $productData
     * @param Mage_Catalog_Model_Product $product
     */
    private function _updatePrice($productData, Mage_Catalog_Model_Product $product)
    {
        if ($productData !== false) {
            $newPrice = Mage::helper('skuio_skugrid')->calculatePrice($product, $productData);
            Mage::getResourceSingleton('catalog/product_action')
                ->updateAttributes(
                    array($product->getId()),
                    array('price' => $newPrice),
                    Mage::app()->getStore()->getStoreId()
                );
            $product->setPrice($newPrice);
        }
    }
}
