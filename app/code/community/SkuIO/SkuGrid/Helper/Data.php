<?php
/**
 * SkuGrid Helper
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class SkuIO_SkuGrid_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Is the module enabled in configuration.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)Mage::getStoreConfig('skugrid/general/enable');
    }

    /**
     * Get API key from configuration.
     *
     * @return string
     */
    public function getApiKey()
    {
        return (string)Mage::getStoreConfig('skugrid/general/api_key');
    }

    /**
     * Is Real Time update enabled
     *
     * @return bool
     */
    public function isRealTime()
    {
        return (bool)Mage::getStoreConfig('skugrid/general/realtime');
    }

    /**
     * Get Default Value for Margin (Percent)
     *
     * @return string
     */
    public function getMarginPercentConfigValue()
    {
        return (string)Mage::getStoreConfig('skugrid/general/margin_percent');
    }

    /**
     * Get Default Value for Margin (Flat)
     *
     * @return string
     */
    public function getMarginFlatConfigValue()
    {
        return (string)Mage::getStoreConfig('skugrid/general/margin_flat');
    }

    /**
     * Get Default Value for Including Shipping In Price
     *
     * @return bool
     */
    public function getIncludingShippingInPriceConfigValue()
    {
        return (bool)Mage::getStoreConfig('skugrid/general/margin_percent');
    }

    /**
     *  Calculate new product price
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $productData
     *
     * @return float
     */
    public function calculatePrice(Mage_Catalog_Model_Product $product, $productData = null)
    {
        $vendorPrice = 0;
        $vendorShipping = 0;
        if ($productData) {
            $vendorPrice = $productData['vendor_price'];
            $vendorShipping = $productData['vendor_shipping'];
        }
        if ($product->getGlobalOverride()) {
            $marginPercent  = $product->getMarginPercent();
            $marginFlat     = $product->getMarginFlat();
            $includeShipping = $product->getIncludeSh();
        } else {
            $marginPercent  = $this->getMarginPercentConfigValue();
            $marginFlat     = $this->getMarginFlatConfigValue();
            $includeShipping = $this->getIncludingShippingInPriceConfigValue();
        }

        // Price formula
        // ([vendor_price] + [vendor_shipping]*[include_sh]) * (100+[margin_percent])/100 + [margin_flat]

        $newPrice = ($vendorPrice + $vendorShipping * $includeShipping) * (100 + $marginPercent)/100 + $marginFlat;

        return $newPrice;
    }
}
