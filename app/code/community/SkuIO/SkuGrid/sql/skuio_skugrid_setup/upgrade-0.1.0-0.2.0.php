<?php
/**
 * SkuGrid
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$attributesData = array(
    'global_override' => array (
        'label' => 'Global Override',
        'type'  => 'int',
        'input' => 'boolean',
    ),
);

foreach ($attributesData as $attributeCode => $attributeData) {
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array(
        'group'             => 'Sku Grid',
        'label'             => $attributeData['label'],
        'input'             => $attributeData['input'],
        'type'              => $attributeData['type'],
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'visible_in_advanced_search' => false,
        'unique'            => false,
        'sort_order'        => 1,
        'default'           => 0,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
}

$installer->endSetup();
