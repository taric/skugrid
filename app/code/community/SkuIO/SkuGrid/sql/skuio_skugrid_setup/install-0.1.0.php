<?php
/**
 * SkuGrid
 *
 * @package    SkuIO_SkuGrid
 * @author     Taras Demianchenko
 * @copyright  Copyright (c) 2016 SkuIO (http://skugrid.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttributeGroup(Mage_Catalog_Model_Product::ENTITY, 'Default', 'Sku Grid', 2);

$attributesData = array(
    'local_id' => array (
        'label' => 'Sku Grid Item Id',
        'type'  => 'text',
        'input' => 'text',
        ),
    'vendor_url' => array (
        'label' => 'Supplier\'s URL',
        'type'  => 'text',
        'input' => 'text',
    ),
    'vendor_variant' => array (
        'label' => 'Supplier\'s Variation',
        'type'  => 'text',
        'input' => 'text',
    ),
    'margin_percent' => array (
        'label' => 'Margin (Percent)',
        'type'  => 'int',
        'input' => 'text',
    ),
    'margin_flat' => array (
        'label' => 'Margin (Flat Value)',
        'type'  => 'int',
        'input' => 'text',
    ),
    'include_sh' => array (
        'label' => 'Including Shipping In Price',
        'type'  => 'int',
        'input' => 'boolean',
    ),
);

foreach ($attributesData as $attributeCode => $attributeData) {
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array(
        'group'             => 'Sku Grid',
        'label'             => $attributeData['label'],
        'input'             => $attributeData['input'],
        'type'              => $attributeData['type'],
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'visible_in_advanced_search' => false,
        'unique'            => false,
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
}

$installer->endSetup();
